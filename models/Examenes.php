<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examenes".
 *
 * @property int $id
 * @property string|null $titulo
 * @property float|null $nota
 * @property string|null $fecha
 * @property int|null $codigoAlumno
 *
 * @property Alumnos $codigoAlumno0
 */
class Examenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nota'], 'number', 'max' => 10, 'min' => 0],
            [['fecha'], 'safe'],
            [['codigoAlumno'], 'integer'],
            [['titulo'], 'string', 'max' => 100],
            [['codigoAlumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::class, 'targetAttribute' => ['codigoAlumno' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'nota' => 'Nota',
            'fecha' => 'Fecha',
            'codigoAlumno' => 'Codigo Alumno',
        ];
    }

    /**
     * Gets query for [[CodigoAlumno0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlumno0()
    {
        return $this->hasOne(Alumnos::class, ['codigo' => 'codigoAlumno']);
    }
}
