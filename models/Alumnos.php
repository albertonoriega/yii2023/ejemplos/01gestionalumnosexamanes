<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $correo
 *
 * @property Examenes[] $examenes
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'correo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre', 'correo'], 'string', 'max' => 100],
            [['correo'], 'unique'],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
        ];
    }

    /**
     * Gets query for [[Examenes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExamenes()
    {
        return $this->hasMany(Examenes::class, ['codigoAlumno' => 'codigo']);
    }
}
