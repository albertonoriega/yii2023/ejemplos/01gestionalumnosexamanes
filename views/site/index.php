<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Gestión de alumnos y exámenes';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Bienvenidos!</h1>

        <p class="lead">Gestión de alumnos</p>


    </div>

    <div>
        <p>Ejercicio inicial en el que creamos una aplicación con yii2 y hacemos su configuración inicial</p>
        <p>Enlazamos la base de datos en la que tenemos dos tablas: alumnos y examenes.</p>
        <p>Creamos los modelos y los CRUD para las dos tablas de la base de datos</p>
        <p>Modificamos el CRUD de las dos tablas para ponerlo en castellano y ponemos iconos en algun botón</p>
        <p>Cambiamos el site/index y ponemos una foto usando el Html helper Img y hacemos que la imagen sea responsive</p>
    </div>

    <?= Html::img(
        "@web/imgs/alumno.jpg",
        [
            'class' => 'img-fluid img-thumbnail',
        ]
    ); ?>
</div>