<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Examenes $model */

$this->title = 'Actualizar exámen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Examenes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Exámen ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="examenes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
